import express = require('express');
const app = express();
const port: number = 3000;

app.get('/', function (req: express.Request, res: express.Response): void {
    res.send('Hello World!');
});

app.get('/posts', function (req, res): void {
    res.send('We are posts :)');
});

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});
